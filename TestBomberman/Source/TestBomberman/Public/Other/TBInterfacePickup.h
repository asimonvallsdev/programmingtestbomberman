// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Core/TBCharacter.h"

#include "TBInterfacePickup.generated.h"


// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UTBInterfacePickup : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class TESTBOMBERMAN_API ITBInterfacePickup
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Pickup")
		void NativePickupEffect(ATBCharacter* character);

};
