// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TBCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FPlayerDeadSignature, int32, id);


class ATBBomb;

UCLASS()
class TESTBOMBERMAN_API ATBCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ATBCharacter();
	
	//Events
	UPROPERTY(BlueprintAssignable, Category = "Events")
		FPlayerDeadSignature OnDead;
	
	//Getters & Setters
	UFUNCTION()
		int32 GetAmmoBomb();
	UFUNCTION()
		void SetAmmoBomb(int value);

	UFUNCTION()
		int32 GetBlastScale();
	UFUNCTION()
		void SetBlastScale(float value);

	UFUNCTION(BlueprintCallable)
		void SetPlayerID(int32 value);

	UFUNCTION(BlueprintCallable)

		int32 GetPlayerID();

	//Pickups Effects
	UFUNCTION()
		void SpeedEffect();

	UFUNCTION()
		void RemoteBombEffect();


	//Damage Functions
	UFUNCTION()
		void TakeDamage(int damage);
	

protected:

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//Movement
	UFUNCTION(BlueprintCallable)
		void MoveForward(float Value);

	UFUNCTION(BlueprintCallable)
		void MoveRight(float Value);

	//Actions
	UFUNCTION(BlueprintCallable)
		void SpawnBomb();

	UFUNCTION()
		void AddAmmoBomb();

	UFUNCTION()
		void DetectPickup(AActor* OverlappedActor, AActor* OtherActor);

	UFUNCTION()
		void RestoreInitSpeed();
	
	UFUNCTION()
		void RestoreRemoteBomb();

	UPROPERTY(VisibleAnywhere, Category = "Multiplayer")
		int32 PlayerID;

	
	FTimerHandle TimerSpeedHandle;

	FTimerHandle TimerRemoteHandle;

	UPROPERTY(EditAnywhere, Category = "Config")
		TSubclassOf<AActor> Bomb;

	UPROPERTY(EditAnywhere, Category = "Gameplay")
		int32 HealthPoints;

	UPROPERTY(EditAnywhere, Category = "Gameplay")
		int32 AmmoBomb;
	
	UPROPERTY(EditAnywhere, Category = "Gameplay")
		float TimeMaxSpeed;

	UPROPERTY(EditAnywhere, Category = "Gameplay")
		float TimeMaxRemote;

	UPROPERTY(EditAnywhere, Category = "Gameplay")
		float InitSpeed;

	UPROPERTY(EditAnywhere, Category = "Gameplay")
		float MaxSpeed;

	UPROPERTY(EditAnywhere, Category = "Pickup Effects")
		float BlastScale;
	
	UPROPERTY(EditAnywhere, Category = "Pickup Effects")
		bool bRemote;

	ATBBomb* CurrentBomb;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
