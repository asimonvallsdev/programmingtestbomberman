// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TBHealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDeadDelegate);
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TESTBOMBERMAN_API UTBHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTBHealthComponent();

	UPROPERTY(BlueprintAssignable, Category = "Events")
		FDeadDelegate OnDead;

	UFUNCTION()
		void TakeDamage(float damage);
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config")
		float Health;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
