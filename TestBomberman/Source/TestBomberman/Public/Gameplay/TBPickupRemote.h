// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gameplay/TBPickup.h"
#include "TBPickupRemote.generated.h"

/**
 * 
 */
UCLASS()
class TESTBOMBERMAN_API ATBPickupRemote : public ATBPickup
{
	GENERATED_BODY()

protected:

	ATBPickupRemote();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void PickupEffect(ATBCharacter* character) override;
public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
