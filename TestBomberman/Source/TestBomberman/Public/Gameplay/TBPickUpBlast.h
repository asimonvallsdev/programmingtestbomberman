// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gameplay/TBPickup.h"

#include "TBPickUpBlast.generated.h"


UCLASS()
class TESTBOMBERMAN_API ATBPickUpBlast : public ATBPickup
{
	GENERATED_BODY()
	

public:	
	// Sets default values for this actor's properties
	ATBPickUpBlast();

	void PickupEffect(ATBCharacter* character) override;

protected:

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;


};
