// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TBDestWall.generated.h"

class UBoxComponent;
class UStaticMeshComponent;

UCLASS()
class TESTBOMBERMAN_API ATBDestWall : public AActor
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, Category = "Components")
		UBoxComponent* BoxCollider;

	UPROPERTY(EditAnywhere, Category = "Components")
		UStaticMeshComponent* StaticMesh;

public:	
	// Sets default values for this actor's properties
	ATBDestWall();

	UFUNCTION()
		void SpawnRandomPickup();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, Category = "Pickups Config")
		TSubclassOf<AActor> PULongerBlast;

	UPROPERTY(EditAnywhere, Category = "Pickups Config")
		TSubclassOf<AActor> PUMoreBombs;

	UPROPERTY(EditAnywhere, Category = "Pickups Config")
		TSubclassOf<AActor> PUFastRunSpeed;

	UPROPERTY(EditAnywhere, Category = "Pickups Config")
		TSubclassOf<AActor> PURemoteBomb;



public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
