// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TBBomb.generated.h"

class UBoxComponent;
class USphereComponent;
class UStaticMeshComponent;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FBombDelegate);

UCLASS()
class TESTBOMBERMAN_API ATBBomb : public AActor
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, Category = "Components")
		USceneComponent* SceneComp;

	UPROPERTY(EditAnywhere, Category = "Components")
		USphereComponent* SphereCollider;

	UPROPERTY(EditAnywhere, Category = "Components")
		UStaticMeshComponent* BombStaticMesh;

	UPROPERTY(EditAnywhere, Category = "Components")
		TArray<UStaticMeshComponent*> BlastStaticMesh;

public:	
	// Sets default values for this actor's properties
	ATBBomb();

	UPROPERTY(BlueprintAssignable, Category = "Events")
		FBombDelegate OnExploted;

	UFUNCTION()
		void SetBlastScale(float value);

	UFUNCTION()
		void ExplosionBlast();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	FTimerHandle TimerExplosionHandle;
	FTimerHandle TimerDestroyHandle;


	UPROPERTY(EditAnywhere, Category = "Timer")
	float ExplosionTime;
	UPROPERTY(EditAnywhere, Category = "Timer")
	float BlastTime;

	UPROPERTY(VisibleAnywhere, Category = "Gameplay")
		FVector BlastScale;

	UFUNCTION()
	void DestroyBombBlast();

	UFUNCTION()
		void OnBlastBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
		UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, 
		const FHitResult& SweepResult);

	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
