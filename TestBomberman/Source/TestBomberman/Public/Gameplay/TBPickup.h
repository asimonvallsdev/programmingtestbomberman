// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Core/TBCharacter.h"
#include "TBPickup.generated.h"

class UBoxComponent;
class UStaticMeshComponent;

UCLASS()
class TESTBOMBERMAN_API ATBPickup : public AActor
{
	GENERATED_BODY()
	
		UPROPERTY(EditAnywhere, Category = "Components")
		UBoxComponent* BoxCollider;

	UPROPERTY(EditAnywhere, Category = "Components")
		UStaticMeshComponent* StaticMesh;

public:	
	// Sets default values for this actor's properties
	ATBPickup();

	UPROPERTY(EditAnywhere, Category = "Gameplay")
		float TimeInvulnerable;

	UPROPERTY(EditAnywhere, Category = "Gameplay")
		bool bVulnerable;


		virtual void PickupEffect(ATBCharacter* character) {};

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	FTimerHandle TimerInvulHandle;

	UFUNCTION()
		void Vulnerable();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
