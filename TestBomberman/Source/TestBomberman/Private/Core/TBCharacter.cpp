// Fill out your copyright notice in the Description page of Project Settings.


#include "Core/TBCharacter.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "Gameplay/TBBomb.h"
#include "Gameplay/TBPickup.h"
#include "GameFramework/CharacterMovementComponent.h"

// Sets default values
ATBCharacter::ATBCharacter()
{
 	
	PrimaryActorTick.bCanEverTick = true;

  GetCharacterMovement()->bOrientRotationToMovement = true; 
  GetCharacterMovement()->RotationRate = FRotator(0.0f, 0.0f, 540.0f);
}


void ATBCharacter::TakeDamage(int damage)
{
  HealthPoints -= damage;

  if (HealthPoints <= 0) {
    OnDead.Broadcast(PlayerID);
  }
}

int32 ATBCharacter::GetAmmoBomb()
{
    return AmmoBomb;
}

void ATBCharacter::SetAmmoBomb(int value)
{
  AmmoBomb = value;
}

int32 ATBCharacter::GetBlastScale()
{
  return BlastScale;
}

void ATBCharacter::SetBlastScale(float value)
{
  BlastScale = value;
}

void ATBCharacter::SpeedEffect()
{
  GetWorldTimerManager().ClearTimer(TimerSpeedHandle);

  GetCharacterMovement()->MaxWalkSpeed = MaxSpeed;

  GetWorld()->GetTimerManager().SetTimer(TimerSpeedHandle, this,
    &ATBCharacter::RestoreInitSpeed, TimeMaxSpeed, false);

}

void ATBCharacter::RemoteBombEffect()
{
  GetWorldTimerManager().ClearTimer(TimerRemoteHandle);

  bRemote = true;
  GetWorld()->GetTimerManager().SetTimer(TimerRemoteHandle, this,
    &ATBCharacter::RestoreRemoteBomb, TimeMaxRemote, false);
}

void ATBCharacter::SetPlayerID(int32 value)
{
  PlayerID = value;
}

int ATBCharacter::GetPlayerID()
{
  return PlayerID;
}

// Called when the game starts or when spawned
void ATBCharacter::BeginPlay()
{
	Super::BeginPlay();
	
  AmmoBomb = 1;
  BlastScale = 1.0f;
  InitSpeed = GetCharacterMovement()->MaxWalkSpeed;
  bRemote = false;

  OnActorBeginOverlap.AddDynamic(this, &ATBCharacter::DetectPickup);
}

void ATBCharacter::MoveForward(float Value)
{
  if (Controller != NULL && (Value != 0.0f)) {
  
    const FRotator Rotation = Controller->GetControlRotation();
    const FRotator YawRotation(0, Rotation.Yaw, 0);

    const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
    AddMovementInput(Direction, Value);
  
  }
}

void ATBCharacter::MoveRight(float Value)
{
  if (Controller != NULL && Value != 0.0f)
  {
    const FRotator Rotation = Controller->GetControlRotation();
    const FRotator YawRotation(0, Rotation.Yaw, 0);

    const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
    AddMovementInput(Direction, Value);
  }
}

void ATBCharacter::SpawnBomb()
{

  if (bRemote) {
  
    if (CurrentBomb != nullptr) {
    
      CurrentBomb->ExplosionBlast();
      AddAmmoBomb();
    }
    else {
      CurrentBomb = Cast<ATBBomb>(GetWorld()->SpawnActor<AActor>(Bomb,
        GetActorLocation(), FRotator(0)));
      CurrentBomb->SetBlastScale(BlastScale);
      CurrentBomb->OnExploted.AddDynamic(this, &ATBCharacter::AddAmmoBomb);
      AmmoBomb--;
    }


  }else if (AmmoBomb >= 1) {
    CurrentBomb = Cast<ATBBomb>(GetWorld()->SpawnActor<AActor>(Bomb,
      GetActorLocation(), FRotator(0)));
    CurrentBomb->SetBlastScale(BlastScale);
    CurrentBomb->OnExploted.AddDynamic(this, &ATBCharacter::AddAmmoBomb);
    AmmoBomb--;
  }

  
}

void ATBCharacter::AddAmmoBomb()
{
  CurrentBomb->OnExploted.RemoveDynamic(this, &ATBCharacter::AddAmmoBomb);
  CurrentBomb = nullptr;
  AmmoBomb++;
}

void ATBCharacter::DetectPickup(AActor* OverlappedActor, AActor* OtherActor)
{
  if (OtherActor != nullptr) {
    
    ATBPickup* pickup = Cast<ATBPickup>(OtherActor);
    if(pickup != nullptr)
    pickup->PickupEffect(this);
  }
}

void ATBCharacter::RestoreInitSpeed()
{
  GetCharacterMovement()->MaxWalkSpeed = InitSpeed;
  GetWorldTimerManager().ClearTimer(TimerSpeedHandle);
}

void ATBCharacter::RestoreRemoteBomb()
{
  bRemote = false;
  GetWorldTimerManager().ClearTimer(TimerRemoteHandle);
}

// Called every frame
void ATBCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ATBCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}


