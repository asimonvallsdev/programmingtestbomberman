// Fill out your copyright notice in the Description page of Project Settings.


#include "Gameplay/TBPickup.h"
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
ATBPickup::ATBPickup()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


	BoxCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollider"));
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));

	RootComponent = BoxCollider;
	StaticMesh->SetupAttachment(RootComponent);
}


// Called when the game starts or when spawned
void ATBPickup::BeginPlay()
{
	Super::BeginPlay();
	
	bVulnerable = false;

	GetWorld()->GetTimerManager().SetTimer(TimerInvulHandle, this,
		&ATBPickup::Vulnerable, TimeInvulnerable, false);

}

void ATBPickup::Vulnerable()
{
	bVulnerable = true;
	GetWorldTimerManager().ClearTimer(TimerInvulHandle);
}

// Called every frame
void ATBPickup::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

