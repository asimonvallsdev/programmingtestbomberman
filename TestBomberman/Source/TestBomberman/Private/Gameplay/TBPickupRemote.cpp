// Fill out your copyright notice in the Description page of Project Settings.


#include "Gameplay/TBPickupRemote.h"
#include "Core/TBCharacter.h"

ATBPickupRemote::ATBPickupRemote() {
  PrimaryActorTick.bCanEverTick = false;
}

void ATBPickupRemote::BeginPlay()
{
  Super::BeginPlay();
}

void ATBPickupRemote::PickupEffect(ATBCharacter* character)
{
  character->RemoteBombEffect();
  Destroy();
}

void ATBPickupRemote::Tick(float DeltaTime)
{
  Super::Tick(DeltaTime);
}
