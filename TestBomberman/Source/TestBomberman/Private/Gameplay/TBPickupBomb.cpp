// Fill out your copyright notice in the Description page of Project Settings.


#include "Gameplay/TBPickupBomb.h"
#include "Core/TBCharacter.h"

// Sets default values
ATBPickupBomb::ATBPickupBomb()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

}

// Called when the game starts or when spawned
void ATBPickupBomb::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATBPickupBomb::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATBPickupBomb::PickupEffect(ATBCharacter* character)
{
	float currentAmmo = character->GetAmmoBomb() + 1.0f;
	character->SetAmmoBomb(currentAmmo);
	Destroy();
}
