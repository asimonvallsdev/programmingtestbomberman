// Fill out your copyright notice in the Description page of Project Settings.


#include "Gameplay/TBPickupRun.h"
#include "Core/TBCharacter.h"

// Sets default values
ATBPickupRun::ATBPickupRun()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

void ATBPickupRun::PickupEffect(ATBCharacter* character)
{
	character->SpeedEffect();
	Destroy();
}

// Called when the game starts or when spawned
void ATBPickupRun::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATBPickupRun::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

