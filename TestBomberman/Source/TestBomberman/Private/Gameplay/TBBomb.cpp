// Fill out your copyright notice in the Description page of Project Settings.


#include "Gameplay/TBBomb.h"

#include "Components/BoxComponent.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Core/TBCharacter.h"
#include "Gameplay/TBDestWall.h"
#include "Gameplay/TBPickup.h"

// Sets default values
ATBBomb::ATBBomb()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
  SceneComp = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComp"));
  SphereCollider = CreateDefaultSubobject<USphereComponent>(TEXT("SphereCollider"));
  BombStaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BombStaticMesh"));

  RootComponent = SceneComp;
  SphereCollider->SetupAttachment(RootComponent);
  BombStaticMesh->SetupAttachment(RootComponent);

  for (int i = 0; i < 4; ++i) {
    FString auxStaticMesh = "BlastStaticMesh"+FString::FromInt(i);
    BlastStaticMesh.Add(CreateDefaultSubobject<UStaticMeshComponent>(FName(*auxStaticMesh)));

    BlastStaticMesh[i]->SetupAttachment(RootComponent);
  }


}

void ATBBomb::SetBlastScale(float value)
{
  BlastScale = FVector(1.0f, value, 1.0f);
}

// Called when the game starts or when spawned
void ATBBomb::BeginPlay()
{
	Super::BeginPlay();
  for (int i = 0; i < 4; ++i) {
    
    BlastStaticMesh[i]->SetHiddenInGame(true);
    BlastStaticMesh[i]->SetSimulatePhysics(false);
    BlastStaticMesh[i]->SetComponentTickEnabled(false);
    BlastStaticMesh[i]->SetCollisionEnabled(ECollisionEnabled::NoCollision);

    BlastStaticMesh[i]->OnComponentBeginOverlap.AddDynamic(this, &ATBBomb::OnBlastBeginOverlap);
  }

  GetWorld()->GetTimerManager().SetTimer(TimerExplosionHandle, this, 
        &ATBBomb::ExplosionBlast, ExplosionTime, false);
}

void ATBBomb::ExplosionBlast()
{
  
  BombStaticMesh->SetHiddenInGame(true);
  BombStaticMesh->SetSimulatePhysics(false);
  BombStaticMesh->SetComponentTickEnabled(false);

  for (int i = 0; i < 4; ++i) {
    BlastStaticMesh[i]->SetRelativeScale3D(BlastScale);
    BlastStaticMesh[i]->SetHiddenInGame(false);
    BlastStaticMesh[i]->SetSimulatePhysics(false);
    BlastStaticMesh[i]->SetComponentTickEnabled(true);
    BlastStaticMesh[i]->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
  }

  GetWorldTimerManager().ClearTimer(TimerExplosionHandle);

  GetWorld()->GetTimerManager().SetTimer(TimerDestroyHandle, this,
    &ATBBomb::DestroyBombBlast, BlastTime, false);

}

void ATBBomb::DestroyBombBlast()
{
  GetWorldTimerManager().ClearTimer(TimerDestroyHandle);
  
  OnExploted.Broadcast();

  Destroy();
}

void ATBBomb::OnBlastBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
  
  if (OtherActor != nullptr) {
 
    ATBPickup* pickup = Cast<ATBPickup>(OtherActor);
    if (pickup != nullptr) {
    
      if (pickup->bVulnerable) {
        pickup->Destroy();
      }
    }

    ATBDestWall* wallDestruc = Cast<ATBDestWall>(OtherActor);
    if (wallDestruc != nullptr) {
      wallDestruc->SpawnRandomPickup();
      wallDestruc->Destroy();
      OverlappedComp->DestroyComponent();
    }

    if (OtherActor->ActorHasTag("Wall")) {
      OverlappedComp->DestroyComponent();
    }


    ATBCharacter* character = Cast<ATBCharacter>(OtherActor);
    if (character != nullptr) {
    
      character->TakeDamage(1);
    }
 
    ATBBomb* otherBomb = Cast<ATBBomb>(OtherActor);
    if (otherBomb != nullptr) {
      otherBomb->ExplosionBlast();
    }

  }
}


// Called every frame
void ATBBomb::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

