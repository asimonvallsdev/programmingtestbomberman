// Fill out your copyright notice in the Description page of Project Settings.


#include "Gameplay/TBDestWall.h"
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
ATBDestWall::ATBDestWall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

  BoxCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollider"));
  StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));

  RootComponent = BoxCollider;
  StaticMesh->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void ATBDestWall::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATBDestWall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATBDestWall::SpawnRandomPickup() {

  //30% probability to spawn a pickup
  int prob = FMath::RandRange(0, 100);

  if ( prob > 70) {
  
    int selec = FMath::RandRange(0, 3);
    
    switch (selec)
    {
    case 0:
      GetWorld()->SpawnActor<AActor>(PULongerBlast, GetActorLocation(), FRotator(0));
      break;
    case 1:
      GetWorld()->SpawnActor<AActor>(PUMoreBombs, GetActorLocation(), FRotator(0));
      break;
    case 2:
      GetWorld()->SpawnActor<AActor>(PUFastRunSpeed, GetActorLocation(), FRotator(0));
      break;
    case 3:
      GetWorld()->SpawnActor<AActor>(PURemoteBomb, GetActorLocation(), FRotator(0));
      break;
    default:
      break;
    }
  
  
  
  }

}

