// Fill out your copyright notice in the Description page of Project Settings.


#include "Gameplay/TBPickUpBlast.h"
#include "Core/TBCharacter.h"


// Sets default values
ATBPickUpBlast::ATBPickUpBlast()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


}



void ATBPickUpBlast::PickupEffect(ATBCharacter* character)
{
	float currentScale = character->GetBlastScale() + 1.0f;
	character->SetBlastScale(currentScale);
	Destroy();
}

// Called when the game starts or when spawned
void ATBPickUpBlast::BeginPlay()
{
	Super::BeginPlay();
	
}


// Called every frame
void ATBPickUpBlast::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

