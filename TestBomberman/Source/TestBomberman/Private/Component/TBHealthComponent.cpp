// Fill out your copyright notice in the Description page of Project Settings.


#include "Component/TBHealthComponent.h"

// Sets default values for this component's properties
UTBHealthComponent::UTBHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}


void UTBHealthComponent::TakeDamage(float damage)
{
	Health -= damage;

	if (Health <= 0) {
		OnDead.Broadcast();
	}
}

// Called when the game starts
void UTBHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UTBHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

