# ProgrammingTestBomberman

Horas: 15


Controles:

-Player 1, Movimiento: W/A/S/D, Accion: Spacebar

-Player 2, Movimiento: Up/Down/Left/Right Arrows, Accion: Enter


Pickups:

- Amarillo: Longer Bomb Blast

- Rosa: Remote Bomb

- Verde: Faster Run Speed

- Naranja: More Bombs
